import { useState, useEffect} from "react";

const ClickCounter = () => {
    const [soLanBam, setSoLanBam] = useState(5);

    const onClickMeButtonClick = () => {
        setSoLanBam(soLanBam + 1);
    }

    useEffect (()=>{
        document.title = "Bạn đã bấm " + soLanBam + " lần";
    },[soLanBam]);
    
    return (
        <div>
            <h2>Ứng dụng đếm số lượt bấm chuột</h2>
            <p>You clicked {soLanBam} times</p>
            <p>
                <button onClick = {onClickMeButtonClick}>Click me</button>
            </p>
        </div>
    )
}

export default ClickCounter;